export class BookingDTO {
  constructor() {
    this.spaceId = "";
    this.clientId = "";
    this.price = 0;
    this.startDate = "";
    this.endDate = "";
  }
}