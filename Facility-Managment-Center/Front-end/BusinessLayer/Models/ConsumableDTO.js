export class ConsumableDTO {
  constructor() {
    this.id = "";
    this.state = "";
    this.name = "";
    this.price = 0;
    this.baseUnit = "";
  }
}
