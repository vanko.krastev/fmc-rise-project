export class Invoice {
  constructor() {
    this.id = "";
    this.invoiceNumber = "";
    this.createdAt = "";
    this.amount = 0;
    this.paid = false;
    this.spaceName = "";
    this.clientName = "";
  }
}
