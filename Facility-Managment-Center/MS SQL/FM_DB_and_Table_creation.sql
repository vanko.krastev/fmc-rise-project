USE [master]
GO
/****** Object:  Database [FM]    Script Date: 09/05/2023 16:33:45 ******/
CREATE DATABASE [FM]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'FM', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\FM.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'FM_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\FM_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [FM] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [FM].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [FM] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [FM] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [FM] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [FM] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [FM] SET ARITHABORT OFF 
GO
ALTER DATABASE [FM] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [FM] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [FM] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [FM] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [FM] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [FM] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [FM] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [FM] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [FM] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [FM] SET  ENABLE_BROKER 
GO
ALTER DATABASE [FM] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [FM] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [FM] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [FM] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [FM] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [FM] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [FM] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [FM] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [FM] SET  MULTI_USER 
GO
ALTER DATABASE [FM] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [FM] SET DB_CHAINING OFF 
GO
ALTER DATABASE [FM] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [FM] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [FM] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [FM] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [FM] SET QUERY_STORE = OFF
GO
USE [FM]
GO
/****** Object:  Table [dbo].[Bookings]    Script Date: 09/05/2023 16:33:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bookings](
	[id] [uniqueidentifier] NOT NULL,
	[space_id] [uniqueidentifier] NOT NULL,
	[client_id] [uniqueidentifier] NOT NULL,
	[start_date] [date] NOT NULL,
	[end_date] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Clients]    Script Date: 09/05/2023 16:33:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Clients](
	[id] [uniqueidentifier] NOT NULL,
	[pin] [nvarchar](255) NOT NULL,
	[name] [nvarchar](255) NOT NULL,
	[number] [nvarchar](255) NULL,
	[email] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Consumables]    Script Date: 09/05/2023 16:33:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Consumables](
	[id] [uniqueidentifier] NOT NULL,
	[name] [nvarchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Inventory]    Script Date: 09/05/2023 16:33:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Inventory](
	[id] [uniqueidentifier] NOT NULL,
	[name] [nvarchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Inventory_consumables]    Script Date: 09/05/2023 16:33:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Inventory_consumables](
	[id] [uniqueidentifier] NOT NULL,
	[price] [decimal](10, 5) NOT NULL,
	[consumable_id] [uniqueidentifier] NOT NULL,
	[inventory_id] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Inventory_consumables] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Invoices]    Script Date: 09/05/2023 16:33:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Invoices](
	[id] [uniqueidentifier] NOT NULL,
	[invoice_number] [nvarchar](255) NOT NULL,
	[client_id] [uniqueidentifier] NOT NULL,
	[space_id] [uniqueidentifier] NOT NULL,
	[paid] [bit] NOT NULL,
	[ammount] [decimal](10, 5) NOT NULL,
	[created_at] [date] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Space_inventory]    Script Date: 09/05/2023 16:33:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Space_inventory](
	[id] [uniqueidentifier] NOT NULL,
	[inventory_id] [uniqueidentifier] NOT NULL,
	[space_id] [uniqueidentifier] NOT NULL,
	[quantity] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Spaces]    Script Date: 09/05/2023 16:33:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Spaces](
	[id] [uniqueidentifier] NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[size] [float] NOT NULL,
	[booked] [bit] NOT NULL,
	[price_per_week] [decimal](10, 5) NULL,
	[price_per_month] [decimal](10, 5) NULL,
 CONSTRAINT [PK__Spaces__3213E83F8B80A7BA] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 09/05/2023 16:33:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[id] [uniqueidentifier] NOT NULL,
	[username] [nvarchar](255) NOT NULL,
	[password] [nvarchar](255) NOT NULL,
	[role] [tinyint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Bookings] ADD  DEFAULT (newid()) FOR [id]
GO
ALTER TABLE [dbo].[Clients] ADD  DEFAULT (newid()) FOR [id]
GO
ALTER TABLE [dbo].[Consumables] ADD  DEFAULT (newid()) FOR [id]
GO
ALTER TABLE [dbo].[Inventory] ADD  DEFAULT (newid()) FOR [id]
GO
ALTER TABLE [dbo].[Invoices] ADD  DEFAULT (newid()) FOR [id]
GO
ALTER TABLE [dbo].[Space_inventory] ADD  DEFAULT (newid()) FOR [id]
GO
ALTER TABLE [dbo].[Spaces] ADD  CONSTRAINT [DF__Spaces__id__36B12243]  DEFAULT (newid()) FOR [id]
GO
ALTER TABLE [dbo].[Spaces] ADD  CONSTRAINT [DF__Spaces__booked__37A5467C]  DEFAULT ((0)) FOR [booked]
GO
ALTER TABLE [dbo].[Users] ADD  DEFAULT (newid()) FOR [id]
GO
ALTER TABLE [dbo].[Bookings]  WITH CHECK ADD FOREIGN KEY([client_id])
REFERENCES [dbo].[Clients] ([id])
GO
ALTER TABLE [dbo].[Bookings]  WITH CHECK ADD  CONSTRAINT [FK__Bookings__space___5070F446] FOREIGN KEY([space_id])
REFERENCES [dbo].[Spaces] ([id])
GO
ALTER TABLE [dbo].[Bookings] CHECK CONSTRAINT [FK__Bookings__space___5070F446]
GO
ALTER TABLE [dbo].[Inventory_consumables]  WITH CHECK ADD  CONSTRAINT [FK_Consumables_Space_inventory] FOREIGN KEY([consumable_id])
REFERENCES [dbo].[Consumables] ([id])
GO
ALTER TABLE [dbo].[Inventory_consumables] CHECK CONSTRAINT [FK_Consumables_Space_inventory]
GO
ALTER TABLE [dbo].[Inventory_consumables]  WITH CHECK ADD  CONSTRAINT [FK_Inventory_consumables_Inventory] FOREIGN KEY([inventory_id])
REFERENCES [dbo].[Inventory] ([id])
GO
ALTER TABLE [dbo].[Inventory_consumables] CHECK CONSTRAINT [FK_Inventory_consumables_Inventory]
GO
ALTER TABLE [dbo].[Invoices]  WITH CHECK ADD FOREIGN KEY([client_id])
REFERENCES [dbo].[Clients] ([id])
GO
ALTER TABLE [dbo].[Invoices]  WITH CHECK ADD  CONSTRAINT [FK__Invoices__space___534D60F1] FOREIGN KEY([space_id])
REFERENCES [dbo].[Spaces] ([id])
GO
ALTER TABLE [dbo].[Invoices] CHECK CONSTRAINT [FK__Invoices__space___534D60F1]
GO
ALTER TABLE [dbo].[Space_inventory]  WITH CHECK ADD FOREIGN KEY([inventory_id])
REFERENCES [dbo].[Inventory] ([id])
GO
ALTER TABLE [dbo].[Space_inventory]  WITH CHECK ADD  CONSTRAINT [FK__Space_inv__space__5441852A] FOREIGN KEY([space_id])
REFERENCES [dbo].[Spaces] ([id])
GO
ALTER TABLE [dbo].[Space_inventory] CHECK CONSTRAINT [FK__Space_inv__space__5441852A]
GO
ALTER TABLE [dbo].[Clients]  WITH CHECK ADD CHECK  (([email] like '%_@__%.__%'))
GO
USE [master]
GO
ALTER DATABASE [FM] SET  READ_WRITE 
GO
