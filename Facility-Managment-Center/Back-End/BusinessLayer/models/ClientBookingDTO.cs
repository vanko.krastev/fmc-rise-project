﻿namespace BusinessLayer.Models;

public class ClientBookingDTO
{
    public Guid Id { get; set; }

    public string Name { get; set; }
}
