﻿namespace BusinessLayer.Models;

public class UserDto
{
    public string Username { get; set; }

    public byte? Role { get; set; }
}
