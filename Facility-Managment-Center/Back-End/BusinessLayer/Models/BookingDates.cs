﻿namespace BusinessLayer.Models;

public class BookingDates
{
    public DateTime? StartDate { get; set; }

    public DateTime? EndDate { get; set; }
}
