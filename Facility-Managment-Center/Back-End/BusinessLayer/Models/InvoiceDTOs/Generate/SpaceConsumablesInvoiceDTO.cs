﻿namespace BusinessLayer.Models.InvoiceDTOs.Generate;

public class SpaceConsumablesInvoiceDTO
{
    public int? Count { get; set; }

    public ConsumableInvoiceDTO Consumables { get; set; }
}
