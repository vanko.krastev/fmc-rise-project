﻿namespace BusinessLayer.Models.InvoiceDTOs.Generate;

public class ConsumableInvoiceDTO
{
    public string Name { get; set; }

    public decimal Price { get; set; }
}
