﻿namespace BusinessLayer.Models.InvoiceDTOs.Generate;

public class ClientInvoiceDTO
{
    public Guid Id { get; set; }

    public string Pin { get; set; }

    public string Name { get; set; }
}
