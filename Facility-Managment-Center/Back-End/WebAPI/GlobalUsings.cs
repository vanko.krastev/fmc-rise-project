// Global using directives

global using BusinessLayer.Mappings;
global using DataLayer.Entities;
global using Microsoft.EntityFrameworkCore;
global using WebAPI;